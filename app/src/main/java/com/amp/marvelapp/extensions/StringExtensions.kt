package com.amp.marvelapp.extensions

import android.util.Patterns
import java.security.MessageDigest


/**
 * Sustituye todos los caracteres especiales de un String por el caracter "_"
 * @return String con los caracteres especiales sustituidos
 */
fun String.removeSpecialCharacters(): String {
    return replace(Regex("[^a-zA-Z0-9.-]"), "_")
}

/**
 * Pone en mayúscula la primera letra de un String
 * @param line String a procesar
 * @return String con la primera letra en mayúscula
 */
fun String?.capitalize(): String {
    if (this == null || this.isEmpty()) return ""
    return Character.toUpperCase(this[0]) + this.substring(1)
}

/**
 * Reemplaza los dígitos por espacios
 */
fun String.onlyNumbers() = this.replace(Regex("[^\\d.]"), "")

/**
 * Comprueba si el texto cumple el formato de email
 * @return resultado comprobación
 */
fun String?.isEmail() = this != null && Patterns.EMAIL_ADDRESS.matcher(this).matches()


/**
 * Elimina [ ] del texto
 */
fun String.removeBrackets(): String = this.replace("[", "").replace("]", "")

/**
 * Obtiene una lista de palabras de un String a partir de los espacios que contiene
 */
fun String.findWords(): List<String> {
    val trimmed = trim().replace(oldValue = " +", newValue = " ")
    return trimmed.split(delimiters = *arrayOf(" "))
}

fun String.md5(): String {
    val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return bytes.joinToString("") {
        "%02x".format(it)
    }
}

fun String.sha1(): String {
    val bytes = MessageDigest.getInstance("SHA-1").digest(this.toByteArray())
    return bytes.joinToString("") {
        "%02x".format(it)
    }
}

fun String.containsLatinLetter(): Boolean = matches(Regex(".*[A-Za-z].*"))
fun String.containsDigit(): Boolean = matches(Regex(".*[0-9].*"))
fun String.isAlphanumeric(): Boolean = matches(Regex("[A-Za-z0-9]*"))
fun String.hasLettersAndDigits(): Boolean = this.containsLatinLetter() && this.containsDigit()
fun String.isIntegerNumber(): Boolean = toIntOrNull() != null
fun String.toDecimalNumber(): Boolean = toDoubleOrNull() != null