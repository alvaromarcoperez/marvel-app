package com.amp.marvelapp.extensions

import java.util.*

/**
 * Devuelve el código del lenguaje especificado en el Lccale o una cadena vacía si no hay un lenguaje definido
 * @return lenguaje
 */
fun lang(): String = Locale.getDefault().language






