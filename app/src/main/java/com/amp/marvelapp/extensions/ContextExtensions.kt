package com.amp.marvelapp.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.DisplayMetrics
import android.view.inputmethod.InputMethodManager
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

/**
 * Obtiene la versión de la aplicación
 * @return La versión de la aplicación o una cadena vacía si hay algún error
 */
fun Context.getAppVersion(): String {
    return try {
        packageManager.getPackageInfo(packageName, 0).versionName
    } catch (e: Exception) {
        ""
    }
}

/**
 * Obtiene el build de la aplicación
 * @return El build de la aplicación o una cadena vacía si hay algún error
 */
fun Context.getAppCode(): String {
    return try {
        packageManager.getPackageInfo(packageName, 0).versionCode.toString()
    } catch (e: Exception) {
        ""
    }
}

/**
 * Lanza un chooser para compartir un contenido a través de otra aplicación
 * @param content Contenido a compartir
 * @param chooserTitle (Opcional) Título del chooser
 */
fun Context.share(content: String, chooserTitle: Int, shareScreenTitle: String = "") {
    val sharingIntent = Intent(Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"
    sharingIntent.putExtra(Intent.EXTRA_TEXT, content)
    if (shareScreenTitle.isEmpty()) {
        startActivity(Intent.createChooser(sharingIntent, resources.getString(chooserTitle)))
    } else {
        startActivity(Intent.createChooser(sharingIntent, shareScreenTitle))
    }
}

/**
 * Lee el contenido de un fichero y lo devuelve como texto plano
 * @param fileName Fichero a leer
 * @return Texto plano resultante de leer el fichero
 */
fun Context.loadAsset(fileName: String): String {
    val strBuilder = StringBuilder()

    safeTry {
        val buffer = BufferedReader(InputStreamReader(assets.open(fileName)))
        var aux = buffer.readLine()
        while (aux != null) {
            strBuilder.append(aux).append("\n")
            aux = buffer.readLine()
        }
    }

    return strBuilder.toString()
}


/**
 * Convierte unidades de dp a px
 */
fun Context.dpToPx(dp: Int): Int {
    val displayMetrics = resources.displayMetrics
    return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

/**
 * Comprueba si hay alguna red activa en el dispositivo (datos o wifi)
 * @return resultado de la comprobación
 */
@SuppressLint("MissingPermission")
fun Context.hasConnectivity(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
}

/**
 * Muestra el teclado
 */
fun Context.showKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

/**
 * Carga un fichero de texto de los assets
 * @param name nombre dle fichero
 * @return json en String
 *
 */
fun Context.loadJSONFromAssets(name: String): String {
    val inputStream = assets.open(name)
    val size = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.read(buffer)
    inputStream.close()

    return String(buffer, Charsets.UTF_8)
}


fun Context.copyAsset(name: String, folderPath: String, grantExecutePermission: Boolean = true) {
    safeTry {
        var fileName = name
        if (name.contains("/")) {
            fileName = name.substring(name.lastIndexOf("/") + 1, name.length)
        }
        val file = File("$folderPath/$fileName")
        if (!file.exists()) {
            assets.open(name).use { input ->
                file.outputStream().buffered().use { output ->
                    input.copyTo(output, 10240)
                }
            }

            if (grantExecutePermission) {
                val processChmod =
                    Runtime.getRuntime().exec("/system/bin/chmod 744 $folderPath/$fileName")
                processChmod.waitFor()
            }
        }
    }
}

/**
 * Muestra un Toast desde un fragmento
 * @param message Mensaje a mostrar en el Toast
 */
fun Context.toast(message: String) {
    android.widget.Toast.makeText(this, message, android.widget.Toast.LENGTH_SHORT).show()
}