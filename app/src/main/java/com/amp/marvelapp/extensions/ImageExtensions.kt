package com.amp.marvelapp.extensions

import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import java.io.File

/**
 * Carga una imagen en un ImageView a través de un fichero local o una url utilizando la librería Glide
 * @param imageUrl Url de la imagen o ruta del fichero local
 */
fun ImageView.loadWithGlide(
    imageUrl: String,
    placeholder: Int = 0,
    cornerRadius: Int = 0,
    placeholderCornerRadius: Int = 0,
    loadSuccess: (() -> Unit)? = null,
    error: (() -> Unit)? = null
) {

    val src = BitmapFactory.decodeResource(resources, placeholder)
    val dr = RoundedBitmapDrawableFactory.create(resources, src)
    dr.cornerRadius = cornerRadius.toFloat()

    val options = com.bumptech.glide.request.RequestOptions()
        .centerCrop()
        .placeholder(dr)
        .priority(com.bumptech.glide.Priority.HIGH)
        .transforms(CenterCrop(), RoundedCorners(placeholderCornerRadius))

    val glide = if (File(imageUrl).exists()) {
        Glide.with(context).load(File(imageUrl))
    } else {
        Glide.with(context).load(imageUrl)
    }

    glide.listener(object : RequestListener<Drawable> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<Drawable>?,
            isFirstResource: Boolean
        ): Boolean {
            error?.invoke()
            return false
        }

        override fun onResourceReady(
            resource: Drawable?,
            model: Any?,
            target: Target<Drawable>?,
            dataSource: com.bumptech.glide.load.DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            loadSuccess?.invoke()
            return false
        }
    }).apply(options).into(this)

}