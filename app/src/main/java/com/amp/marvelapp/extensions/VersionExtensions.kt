package com.amp.marvelapp.extensions

import android.os.Build

fun hasLollipop(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
fun hasMarshmallow() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
fun hasNougat(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
fun hasOreo() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
fun hasPie() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
fun hasQ() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
