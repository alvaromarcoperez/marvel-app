package com.amp.marvelapp.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Devuelve si la vista muestra el ultimo elemento, es decir, está en el final
 * @return resultado comprobación
 */
fun RecyclerView.isLastItemVisible(): Boolean {
    if (layoutManager is LinearLayoutManager) {
        return (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() == (adapter?.itemCount
            ?: 0) - 1
    } else {
        TODO("Not implemented. Required LinearLayout Manager")
    }
}

/**
 * Devuelve si la vista muestra el primer elemento por completo, es decir, si el scroll está al principio
 * @return resultado comprobación
 */
fun RecyclerView.isFirstItemVisible(): Boolean {
    if (layoutManager is LinearLayoutManager) {
        return (layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0
    } else {
        TODO("Not implemented. Required LinearLayout Manager")
    }
}

/**
 * Devuelve si la vista muestra el primer elemento por completo, es decir, si el scroll está al principio
 * @return resultado comprobación
 */
fun RecyclerView.isPositionVisible(position: Int): Boolean {
    if (layoutManager is LinearLayoutManager) {
        val firstPosition =
            (layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition()
        val lastPosition =
            (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
        return position in firstPosition..lastPosition
    } else {
        TODO("Not implemented. Required LinearLayout Manager")
    }
}

