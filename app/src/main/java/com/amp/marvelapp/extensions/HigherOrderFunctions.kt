package com.amp.marvelapp.extensions

import android.os.Handler

/**
 * Ejecuta una función transcurrido un tiempo especificado
 * @param ms El tiempo en milisegundos
 * @param lambda La función a ejecutar
 */
fun after(ms: Long, lambda: () -> Unit): Handler {
    val h = Handler()
    h.postDelayed({ lambda() }, ms)
    return h
}

/**
 * Ejecuta un bloque de código capturando cualquier excepción, imprime su traza y evita que la excepción se progague
 * @param block El bloque de código a ejecutar
 */
inline fun safeTry(block: () -> Unit) {
    try {
        block()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}