package com.amp.marvelapp.extensions

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

/**
 * Abre una determinada URL en un navegador externo
 * @param url la URL a abrir
 */
fun AppCompatActivity.openWebView(url: String) {
    if (url.isNotEmpty()) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}

/**
 * Carga un fragmento en un contendor
 * @param fragment El fragmento a cargar
 * @param container El contenedor
 */
fun AppCompatActivity.loadFragment(
    fragment: Class<out Fragment>,
    container: Int,
    addToBackStack: Boolean = false,
    tag: String? = null
) {
    val ft = this.supportFragmentManager.beginTransaction().replace(container, fragment, null)
    if (addToBackStack) {
        ft.addToBackStack(tag)
    }
    ft.commitAllowingStateLoss()
}

