package com.amp.marvelapp.extensions

/**
 * Transforma la entrada en días
 */
fun Int.toDays() = this.toLong() * 24 * 60 * 60 * 1000

/**
 * Transforma la entrada en horas
 */
fun Int.toHours() = this.toLong() * 60 * 60 * 1000

/**
 * Transforma la entrada en minutos
 */
fun Int.toMinutes() = this.toLong() * 60 * 1000

/**
 * Transforma bytes a megas
 */
fun Long.bytesToMb(): Float = if (this == 0L) {
    0F
} else this / 1024F / 1024F


