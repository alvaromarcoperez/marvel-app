package com.amp.marvelapp.extensions

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import androidx.annotation.RequiresApi
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

/**
 * Convierte un timestamp en el formato de fecha indicado
 * @param dateFormat (Opcional) Formato de la fecha
 */
fun Long.toDate(dateFormat: String = "dd/MM/yyyy"): String {
    safeTry {
        return SimpleDateFormat(dateFormat, Locale.getDefault()).format(Date(this))
    }
    return "-"
}

/**
 * Convierte de Device-independent pixels a pixels
 * @param context El contexto
 * @param dp Valor en Dp
 * @return Valor en Pixels
 */
fun dpToPx(context: Context, dp: Int): Int {
    val displayMetrics = context.resources.displayMetrics
    return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}

/**
 * Convierte un booleano a un entero
 * @return 1 si el booleano es true, 0 en caso contrario
 */
fun Boolean.toInt() = if (this) 1 else 0

/**
 * Convierte un entero a boleano
 * @return false si el entero es igual a 0, true en caso contrario
 */
fun Int.toBoolean(): Boolean = this != 0

/**
 * Convierte un array de bytes en un String
 */
@RequiresApi(Build.VERSION_CODES.KITKAT)
fun ByteArray.convertToString(): String {
    return String(this, StandardCharsets.UTF_8)
}

/**
 * Convierte un valor numerico en Device-independent pixels a pixels
 * @param context El contexto
 * @return Valor en pixels
 */
fun Number.dpToPx(context: Context): Float {
    return this.toFloat() * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

/**
 * Convierte un valor numerico en pixels a Device-independent pixels
 * @param context El contexto
 * @return Valor en Device-independent pixels
 */
fun Number.pxToDp(context: Context): Int {
    return (this.toFloat() / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}

/**
 * Convierte un valor numerico a String formateando con un 0 a la izquierda aquellos valores de un solo dígito
 * @return valor formateado
 */
fun Number.format2Digits(): String {
    return if (this.toInt() < 10) {
        ("0$this")
    } else {
        this.toString()
    }
}


/**
 * Convierte array de bytes en hexadecimal
 * @return Valor hexadecimal
 */
fun ByteArray.byteToHex(): String {
    val sb = StringBuilder()
    for (b in this) {
        sb.append(String.format("%02X", b))
    }
    return sb.toString()
}