package com.amp.marvelapp.ui.features.top.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.amp.marvelapp.R
import com.amp.marvelapp.ui.core.base.BaseFragment
import com.amp.marvelapp.ui.features.characters.activity.CharacterListActivity
import com.amp.marvelapp.ui.features.comics.activity.ComicListActivity
import com.amp.marvelapp.ui.features.main.activity.MainActivity
import com.amp.marvelapp.ui.features.top.adapter.TopCharactersAdapter
import com.amp.marvelapp.ui.features.top.adapter.TopComicsAdapter
import com.amp.marvelapp.ui.features.top.viewModel.TopCharactersState
import com.amp.marvelapp.ui.features.top.viewModel.TopComicsState
import com.amp.marvelapp.ui.features.top.viewModel.TopViewModel
import kotlinx.android.synthetic.main.content_top_characters.*
import kotlinx.android.synthetic.main.content_top_comics.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopFragment : BaseFragment(R.layout.fragment_top) {

    private val viewModel: TopViewModel by viewModel()

    private lateinit var characterAdapter: TopCharactersAdapter
    private lateinit var comicAdapter: TopComicsAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupCharacterList()
        setupComicList()

        viewModel.listenTops()

    }

    override fun setObservers() {
        viewModel.topCharacterState.observe(this, Observer { topCharacterState ->
            when (topCharacterState) {
                is TopCharactersState.Success -> {
                    placeholder_empty_characters.visibility = View.GONE
                    top_characters_list.visibility = View.VISIBLE
                    characterAdapter.setData(topCharacterState.characters)
                }

                is TopCharactersState.Empty -> {
                    placeholder_empty_characters.visibility = View.VISIBLE
                    top_characters_list.visibility = View.GONE
                }

                is TopCharactersState.Error -> {
                    placeholder_empty_characters.visibility = View.VISIBLE
                    top_characters_list.visibility = View.GONE
                    (activity as MainActivity).showSnackbar(getString(R.string.error_top_characters))
                }
            }
        })

        viewModel.topComicsState.observe(this, Observer { topComicState ->
            when (topComicState) {
                is TopComicsState.Success -> {
                    placeholder_empty_comics.visibility = View.GONE
                    top_comics_list.visibility = View.VISIBLE
                    comicAdapter.setData(topComicState.comics)
                }

                is TopComicsState.Empty -> {
                    placeholder_empty_comics.visibility = View.VISIBLE
                    top_comics_list.visibility = View.GONE
                }

                is TopComicsState.Error -> {
                    placeholder_empty_comics.visibility = View.VISIBLE
                    top_comics_list.visibility = View.GONE
                    (activity as MainActivity).showSnackbar(getString(R.string.error_top_comics))

                }
            }
        })

    }

    override fun setListeners() {
        view_all_characters.setOnClickListener {
            startActivity(Intent(activity, CharacterListActivity::class.java))
        }

        view_all_comics.setOnClickListener {
            startActivity(Intent(activity, ComicListActivity::class.java))
        }
    }


    private fun setupCharacterList() {
        characterAdapter = TopCharactersAdapter() { character ->

        }
        top_characters_list.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        top_characters_list.adapter = characterAdapter
    }

    private fun setupComicList() {
        comicAdapter = TopComicsAdapter() { comic ->

        }

        top_comics_list.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        top_comics_list.adapter = comicAdapter


    }

}