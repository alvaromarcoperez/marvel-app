package com.amp.marvelapp.ui.features.top.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.amp.marvelapp.R
import com.amp.marvelapp.domain.features.comics.models.ComicTopView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.adapter_top_comics.view.*

class TopComicsAdapter(
    var items: MutableList<ComicTopView> = mutableListOf(),
    private val listener: (ComicTopView) -> Unit
) : RecyclerView.Adapter<TopComicsAdapter.TopViewHolder>() {


    fun setData(newItems: List<ComicTopView>) {
        val diffCallback = TopDiffCallback(items, newItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(newItems)

        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopViewHolder =
        TopViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_top_comics, parent, false)
        )

    override fun onBindViewHolder(holder: TopViewHolder, position: Int) =
        holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class TopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ComicTopView?, listener: (ComicTopView) -> Unit) {
            item?.let {
                itemView.name.text = item.name

                if (item.isFavorite) {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_on)
                } else {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_off)
                }

                Glide
                    .with(itemView)
                    .load(item.thumbnail)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .centerCrop()
                    .placeholder(android.R.color.black)
                    .error(android.R.drawable.ic_delete)
                    /*.addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.e("Error loading image: $e")
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.d("loading image OK")

                            return false
                        }

                    })*/
                    .into(itemView.image)

                itemView.icon_favorite.setOnClickListener { listener(item) }

                itemView.setOnClickListener { listener(item) }
            }
        }

    }

    class TopDiffCallback(
        private val oldList: List<ComicTopView>,
        private val newList: List<ComicTopView>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}
