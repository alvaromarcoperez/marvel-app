package com.amp.marvelapp.ui.features.main.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amp.marvelapp.domain.features.user.IUserRepository
import com.amp.marvelapp.domain.features.user.models.User
import com.amp.marvelapp.ui.core.base.BaseViewModel
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.coroutines.launch

class MainViewModel(private val userRepository: IUserRepository) : BaseViewModel() {

    private val _userState = MutableLiveData<UserState>()
    val userState: LiveData<UserState> get() = _userState

    private val _loginState = MutableLiveData<LoginState>()
    val loginState: LiveData<LoginState> get() = _loginState

    fun getUser() {
        launch {
            userRepository.getUser()
                .onSuccess { _userState.postValue(UserState.Success(it)) }
                .onFailure { _userState.postValue(UserState.NotFound) }

        }
    }

    fun authenticateFirebase(account: GoogleSignInAccount?) {
        account?.let {
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            launch {
                userRepository.login(credential)
                    .onSuccess { _loginState.postValue(LoginState.Success(it)) }
                    .onFailure { _loginState.postValue(LoginState.Error) }
            }
        } ?: run {
            _loginState.postValue(LoginState.Error)
        }
    }

}

sealed class UserState {
    class Success(val user: User) : UserState()
    object NotFound : UserState()
}

sealed class LoginState {
    class Success(val user: User) : LoginState()
    object Error : LoginState()
}