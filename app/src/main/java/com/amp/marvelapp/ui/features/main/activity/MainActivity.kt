package com.amp.marvelapp.ui.features.main.activity

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.amp.marvelapp.R
import com.amp.marvelapp.domain.features.user.models.User
import com.amp.marvelapp.extensions.loadFragment
import com.amp.marvelapp.ui.core.base.BaseActivity
import com.amp.marvelapp.ui.core.utils.showSnackbar
import com.amp.marvelapp.ui.features.favorites.fragment.FavoritesFragment
import com.amp.marvelapp.ui.features.main.viewModel.LoginState
import com.amp.marvelapp.ui.features.main.viewModel.MainViewModel
import com.amp.marvelapp.ui.features.main.viewModel.UserState
import com.amp.marvelapp.ui.features.top.fragment.TopFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class MainActivity : BaseActivity(R.layout.activity_main) {

    private val viewModel: MainViewModel by viewModel()

    private var userItemMenu: MenuItem? = null

    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)

        setupGoogleSignIn()


        bottom_navigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener)
        loadFragment(TopFragment::class.java, R.id.container)

        viewModel.getUser()
    }

    override fun onStart() {
        super.onStart()

        //val account = GoogleSignIn.getLastSignedInAccount(this)

        //Toast.makeText(this, "YET REGISTERED: ${account.toString()}", Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        userItemMenu = menu?.findItem(R.id.item_user)

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.item_user -> {
            login()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)

                viewModel.authenticateFirebase(account)
            } catch (exception: ApiException) {
                // Google Sign In failed, update UI appropriately
                Timber.d("Google sign in failed - $exception")
            }
        }
    }

    override fun setObservers() {
        viewModel.userState.observe(this, Observer { userState ->
            when (userState) {
                is UserState.Success -> {
                    login_layout.visibility = View.GONE
                    loadUser(userState.user)
                }

                is UserState.NotFound -> {
                    login_layout.visibility = View.VISIBLE
                }
            }
        })

        viewModel.loginState.observe(this, Observer { loginState ->
            when (loginState) {
                is LoginState.Success -> {
                    login_layout.visibility = View.GONE
                    loadUser(loginState.user)
                }

                is LoginState.Error -> {
                    login_layout.visibility = View.VISIBLE
                    Toast.makeText(this, "LOGIN ERROR", Toast.LENGTH_SHORT).show()
                }
            }
        })

    }

    override fun setListeners() {
        googl_sign_in.setOnClickListener {
            val signInIntent: Intent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

    }

    private fun setupGoogleSignIn() {
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }


    private var navigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.navigation_home -> {
                        loadFragment(TopFragment::class.java, R.id.container)
                        return true
                    }
                    R.id.navigation_favorites -> {
                        loadFragment(FavoritesFragment::class.java, R.id.container)
                        return true
                    }
                }
                return false
            }
        }

    private fun login() {

    }

    private fun loadUser(user: User) {
        loadImage(user.avatar)
    }

    private fun loadImage(thumbnail: String) {
        Glide.with(this)
            .load(thumbnail)
            .circleCrop()
            .into(object : CustomTarget<Drawable>() {
                override fun onLoadCleared(placeholder: Drawable?) {

                }

                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    userItemMenu?.icon = resource
                }
            })

    }

    fun showSnackbar(message: String) {
        bottom_navigation.showSnackbar(message)
    }


    companion object {
        const val RC_SIGN_IN = 100
        fun callingIntent(context: Context) = Intent(context, MainActivity::class.java)
    }
}
