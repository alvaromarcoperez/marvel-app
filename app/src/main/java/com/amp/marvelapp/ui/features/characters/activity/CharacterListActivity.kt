package com.amp.marvelapp.ui.features.characters.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.amp.marvelapp.R
import com.amp.marvelapp.data.core.utils.NetworkState
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.amp.marvelapp.domain.features.comics.models.ComicView
import com.amp.marvelapp.extensions.after
import com.amp.marvelapp.ui.core.base.BaseActivity
import com.amp.marvelapp.ui.features.characters.adapter.CharacterActions
import com.amp.marvelapp.ui.features.characters.adapter.CharacterAdapter
import com.amp.marvelapp.ui.features.characters.viewModel.CharacterListViewModel
import kotlinx.android.synthetic.main.activity_character_list.*
import kotlinx.android.synthetic.main.toolbar_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class CharacterListActivity : BaseActivity(R.layout.activity_character_list) {

    private val viewModel: CharacterListViewModel by viewModel()

    private lateinit var adapter: CharacterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar_search)
        setupRefresh()
        initList()

        viewModel.showCharacters()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_search, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView?

        searchView?.imeOptions = EditorInfo.IME_ACTION_SEARCH

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                searchView.setQuery("", false)
                searchItem?.collapseActionView()
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                after(300) { newText?.let { viewModel.showCharacters(it) } }
                return false
            }

        })

        return true
    }

    override fun setObservers() {
        viewModel.networkState.observe(this, Observer {
            Timber.d("networkState - STATUS: ${it.status} - MSG: ${it.msg}")
        })

        viewModel.refreshState.observe(this, Observer {
            refresh.isRefreshing = it == NetworkState.LOADING
        })

        viewModel.characters.observe(this, Observer {
            adapter.submitList(it)
        })
    }

    override fun setListeners() {
        refresh.setOnRefreshListener { viewModel.refresh() }
    }

    private fun initList() {
        adapter = CharacterAdapter { action ->
            when (action) {
                is CharacterActions.CharacterClick -> {
                    loadDetail(action.character)
                }

                is CharacterActions.FavoriteClick -> {
                    viewModel.doFavorite(action.character)
                }
            }
        }

        character_list.layoutManager = GridLayoutManager(this, 2)
        character_list.adapter = adapter

        character_list.visibility = View.VISIBLE
    }

    private fun setupRefresh() {
        refresh.setColorSchemeResources(R.color.colorPrimary)
        refresh.setProgressBackgroundColorSchemeResource(R.color.colorAccent)
    }

    private fun loadDetail(character : CharacterView){

    }
}