package com.amp.marvelapp.ui.features.comics.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.amp.marvelapp.R
import com.amp.marvelapp.domain.features.comics.models.ComicView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.adapter_comic.view.*

class ComicAdapter(private val listener: (ComicActions) -> Unit) :
    PagedListAdapter<ComicView, ComicAdapter.ComicHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicHolder =
        ComicHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_comic, parent, false)
        )

    override fun onBindViewHolder(holder: ComicHolder, position: Int) =
        holder.bind(getItem(position), listener)

    inner class ComicHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: ComicView?, listener: (ComicActions) -> Unit) {
            item?.let {
                itemView.name.text = item.title

                if (item.isFavorite) {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_on)
                } else {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_off)
                }

                Glide
                    .with(itemView)
                    .load(item.thumbnail)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .centerCrop()
                    .placeholder(android.R.color.black)
                    .error(android.R.drawable.ic_delete)
                    /*.addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.e("Error loading image: $e")
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.d("loading image OK")

                            return false
                        }

                    })*/
                    .into(itemView.image)

                itemView.icon_favorite.setOnClickListener {
                    listener(ComicActions.FavoriteClick(item))
                }

                itemView.setOnClickListener {
                    listener(ComicActions.ComicClick(item))
                }
            }
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ComicView>() {
            override fun areItemsTheSame(oldItem: ComicView, newItem: ComicView): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: ComicView, newItem: ComicView): Boolean =
                oldItem == newItem
        }
    }
}

sealed class ComicActions {
    class ComicClick(val comic: ComicView) : ComicActions()
    class FavoriteClick(val comic: ComicView) : ComicActions()
}


