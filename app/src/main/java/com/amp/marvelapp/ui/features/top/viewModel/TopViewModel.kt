package com.amp.marvelapp.ui.features.top.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amp.marvelapp.domain.features.characters.ICharactersRepository
import com.amp.marvelapp.domain.features.characters.models.CharaterTopView
import com.amp.marvelapp.domain.features.comics.IComicsRepository
import com.amp.marvelapp.domain.features.comics.models.ComicTopView
import com.amp.marvelapp.ui.core.base.BaseViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

class TopViewModel(
    private val charactersRepository: ICharactersRepository,
    private val comicsRepository: IComicsRepository
) : BaseViewModel() {

    private val _topCharacterState = MutableLiveData<TopCharactersState>()
    val topCharacterState: LiveData<TopCharactersState> get() = _topCharacterState

    private val _topComicsState = MutableLiveData<TopComicsState>()
    val topComicsState: LiveData<TopComicsState> get() = _topComicsState

    fun listenTops() {
        listenTopCharacters()
        listenTopComics()
    }

    private fun listenTopCharacters() {
        launch {
            charactersRepository.getTopCharacters().collect { result ->
                result.fold(
                    onSuccess = { charaters ->
                        Timber.d(charaters.toString())
                        if (charaters.isNotEmpty()) {
                            _topCharacterState.postValue(TopCharactersState.Success(charaters))
                        } else {
                            _topCharacterState.postValue(TopCharactersState.Empty)
                        }
                    },
                    onFailure = {
                        it.printStackTrace()
                        _topCharacterState.postValue(TopCharactersState.Error)
                    }
                )

            }
        }
    }

    private fun listenTopComics() {
        launch {
            comicsRepository.getTopComics().collect { result ->
                result.fold(
                    onSuccess = { comics ->
                        Timber.d(comics.toString())
                        if (comics.isNotEmpty()) {
                            _topComicsState.postValue(TopComicsState.Success(comics))
                        } else {
                            _topComicsState.postValue(TopComicsState.Empty)
                        }
                    },
                    onFailure = {
                        it.printStackTrace()
                        _topComicsState.postValue(TopComicsState.Error)
                    }
                )
            }
        }
    }
}

sealed class TopCharactersState {
    class Success(var characters: List<CharaterTopView>) : TopCharactersState()
    object Empty : TopCharactersState()
    object Error : TopCharactersState()
}

sealed class TopComicsState {
    class Success(var comics: List<ComicTopView>) : TopComicsState()
    object Empty : TopComicsState()
    object Error : TopComicsState()
}