package com.amp.marvelapp.ui.features.comics.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.paging.PagedList
import com.amp.marvelapp.data.core.utils.NetworkState
import com.amp.marvelapp.domain.features.comics.IComicsRepository
import com.amp.marvelapp.domain.features.comics.models.ComicView
import com.amp.marvelapp.ui.core.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

class ComicListViewModel(private val repository: IComicsRepository) : BaseViewModel() {

    private val startWith = MutableLiveData<String?>()

    private val repoResult = startWith.map {
        repository.getComics(CoroutineScope(coroutineContext), it)
    }

    val comics: LiveData<PagedList<ComicView>> = repoResult.switchMap { it.pagedList }
    val networkState: LiveData<NetworkState> = repoResult.switchMap { it.networkState }
    val refreshState: LiveData<NetworkState> = repoResult.switchMap { it.refreshState }

    fun showComics(searchName: String? = null): Boolean {
        if (startWith.value != null && startWith.value == searchName) {
            return false
        }
        startWith.value = searchName
        return true
    }

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun retry() {
        val listing = repoResult.value
        listing?.retry?.invoke()
    }

    fun doFavorite(comic: ComicView) {
        launch {
            repository.doFavorite(comic)
                .onSuccess {
                    Timber.d(it.toString())
                }
                .onFailure {
                    it.printStackTrace()
                    Timber.d("error")

                }
        }
    }
}
