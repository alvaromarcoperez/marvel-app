package com.amp.marvelapp.ui.core.di

import com.amp.marvelapp.ui.features.characters.viewModel.CharacterListViewModel
import com.amp.marvelapp.ui.features.comics.viewModel.ComicListViewModel
import com.amp.marvelapp.ui.features.favorites.fragment.FavoritesFragment
import com.amp.marvelapp.ui.features.favorites.viewModel.FavoritesViewModel
import com.amp.marvelapp.ui.features.main.viewModel.MainViewModel
import com.amp.marvelapp.ui.features.top.fragment.TopFragment
import com.amp.marvelapp.ui.features.top.viewModel.TopViewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val fragmentModule = module {
    fragment { TopFragment() }
    fragment { FavoritesFragment() }
}

val viewModelModule = module {
    viewModel { MainViewModel(userRepository = get()) }
    viewModel { TopViewModel(charactersRepository = get(), comicsRepository = get()) }
    viewModel { FavoritesViewModel() }
    viewModel { CharacterListViewModel(repository = get()) }
    viewModel { ComicListViewModel(repository = get()) }
    //viewModel { (idLeague: String) -> CommentsViewModel(idLeague, commentsRepository = get(), commentsApi = get()) }

}



