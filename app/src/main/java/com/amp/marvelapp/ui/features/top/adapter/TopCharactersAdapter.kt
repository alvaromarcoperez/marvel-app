package com.amp.marvelapp.ui.features.top.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.amp.marvelapp.R
import com.amp.marvelapp.domain.features.characters.models.CharaterTopView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.adapter_top_character.view.*

class TopCharactersAdapter(
    var items: MutableList<CharaterTopView> = mutableListOf(),
    private val listener: (CharaterTopView) -> Unit
) : RecyclerView.Adapter<TopCharactersAdapter.CharacterViewHolder>() {


    fun setData(newItems: List<CharaterTopView>) {
        val diffCallback = CharacterDiffCallback(items, newItems)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(newItems)

        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder =
        CharacterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_top_character, parent, false)
        )

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) =
        holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: CharaterTopView?, listener: (CharaterTopView) -> Unit) {
            item?.let {
                itemView.name.text = item.name

                if (item.isFavorite) {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_on)
                } else {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_off)
                }

                Glide
                    .with(itemView)
                    .load(item.thumbnail)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .centerCrop()
                    .placeholder(android.R.color.black)
                    .error(android.R.drawable.ic_delete)
                    /*.addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.e("Error loading image: $e")
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.d("loading image OK")

                            return false
                        }

                    })*/
                    .into(itemView.image)

                itemView.icon_favorite.setOnClickListener { listener(item) }

                itemView.setOnClickListener { listener(item) }
            }
        }

    }

    class CharacterDiffCallback(
        private val oldList: List<CharaterTopView>,
        private val newList: List<CharaterTopView>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}