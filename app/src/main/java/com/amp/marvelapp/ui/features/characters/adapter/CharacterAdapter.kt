package com.amp.marvelapp.ui.features.characters.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.amp.marvelapp.R
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import kotlinx.android.synthetic.main.adapter_character.view.*


class CharacterAdapter(
    private val listener: (CharacterActions) -> Unit
) : PagedListAdapter<CharacterView, CharacterAdapter.CharacterHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterHolder =
        CharacterHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_character, parent, false)
        )

    override fun onBindViewHolder(holder: CharacterHolder, position: Int) =
        holder.bind(getItem(position), listener)

    inner class CharacterHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: CharacterView?, listener: (CharacterActions) -> Unit) {
            item?.let {
                itemView.name.text = item.name

                if (item.isFavorite) {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_on)
                } else {
                    itemView.icon_favorite.setImageResource(android.R.drawable.star_big_off)
                }

                Glide
                    .with(itemView)
                    .load(item.thumbnail)
                    .transition(withCrossFade())
                    .centerCrop()
                    .placeholder(android.R.color.black)
                    .error(android.R.drawable.ic_delete)
                    /*.addListener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.e("Error loading image: $e")
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.d("loading image OK")

                            return false
                        }

                    })*/
                    .into(itemView.image)

                itemView.icon_favorite.setOnClickListener {
                    listener(CharacterActions.FavoriteClick(item))
                }

                itemView.setOnClickListener {
                    listener(CharacterActions.CharacterClick(item))
                }
            }
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<CharacterView>() {
            override fun areItemsTheSame(oldItem: CharacterView, newItem: CharacterView): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: CharacterView, newItem: CharacterView): Boolean =
                oldItem == newItem
        }
    }
}

sealed class CharacterActions {
    class CharacterClick(val character: CharacterView) : CharacterActions()
    class FavoriteClick(val character: CharacterView) : CharacterActions()
}


