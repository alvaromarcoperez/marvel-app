package com.amp.marvelapp.ui.features.characters.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.paging.PagedList
import com.amp.marvelapp.data.core.utils.NetworkState
import com.amp.marvelapp.domain.features.characters.ICharactersRepository
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.amp.marvelapp.ui.core.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

class CharacterListViewModel(private val repository: ICharactersRepository) : BaseViewModel() {


    private val startWith = MutableLiveData<String?>()

    private val repoResult = startWith.map {
        repository.getCharacters(CoroutineScope(coroutineContext), it)
    }

    val characters: LiveData<PagedList<CharacterView>> = repoResult.switchMap { it.pagedList }
    val networkState: LiveData<NetworkState> = repoResult.switchMap { it.networkState }
    val refreshState: LiveData<NetworkState> = repoResult.switchMap { it.refreshState }

    fun showCharacters(searchName: String? = null): Boolean {
        if (startWith.value != null && startWith.value == searchName) {
            return false
        }
        startWith.value = searchName
        return true
    }

    fun refresh() {
        repoResult.value?.refresh?.invoke()
    }

    fun retry() {
        val listing = repoResult.value
        listing?.retry?.invoke()
    }

    fun doFavorite(character: CharacterView) {
        launch {
            repository.doFavorite(character)
                .onSuccess {
                    Timber.d(it.toString())
                }
                .onFailure {
                    it.printStackTrace()
                    Timber.d("error")

                }
        }

    }

    /*fun getExample() {
        launch {
            repository.getExample(null).fold(
                onSuccess = {
                    Timber.d("ALVAROTEST - SUCCESS")
                },
                onFailure = {
                    Timber.d("ALVAROTEST - ERROR")
                }
            )
        }
    }*/
}