package com.amp.marvelapp.ui.core.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import org.koin.androidx.fragment.android.setupKoinFragmentFactory

abstract class BaseActivity(@LayoutRes var layoutRes : Int) : AppCompatActivity(layoutRes) {

    open fun setListeners() {}

    open fun setObservers() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupKoinFragmentFactory()

        setListeners()
        setObservers()
    }

}