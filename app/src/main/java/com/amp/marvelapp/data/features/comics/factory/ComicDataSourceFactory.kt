package com.amp.marvelapp.data.features.comics.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.amp.marvelapp.data.features.comics.service.ComicService
import com.amp.marvelapp.domain.features.comics.models.ComicView
import kotlinx.coroutines.CoroutineScope

class ComicDataSourceFactory(
    private val coroutineScope: CoroutineScope,
    private val service: ComicService,
    private val titleStartsWith: String?
) :
    DataSource.Factory<Int, ComicView>() {

    val sourceLiveData = MutableLiveData<ComicDataSource>()

    override fun create(): DataSource<Int, ComicView> {
        val source =
            ComicDataSource(
                coroutineScope,
                service,
                titleStartsWith
            )
        sourceLiveData.postValue(source)
        return source
    }
}