package com.amp.marvelapp.data.features.characters.cache

import androidx.room.Dao
import androidx.room.Query
import com.amp.marvelapp.data.core.room.BaseDao
import com.amp.marvelapp.data.features.characters.models.CharacterEntity

@Dao
interface CharacterDao : BaseDao<CharacterEntity> {

    @Query("SELECT * from CharacterEntity")
    suspend fun getCharacters() : List<CharacterEntity>

    @Query("SELECT * from CharacterEntity WHERE id = :id")
    suspend fun getById(id: String): CharacterEntity?

    @Query("DELETE FROM CharacterEntity WHERE id = :id")
    suspend fun deleteById(id: String)

}