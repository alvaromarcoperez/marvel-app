package com.amp.marvelapp.data.features.comics.cache

import com.amp.marvelapp.data.features.comics.models.ComicEntity

class ComicCache(private val comicDao: ComicDao) {

    suspend fun saveComic(comicEntity: ComicEntity) {
        comicDao.insert(comicEntity)
    }

    suspend fun deleteComic(comicEntity: ComicEntity) {
        comicDao.delete(comicEntity)
    }

    suspend fun deleteComicById(characterId: String) {
        comicDao.deleteById(characterId)
    }

    suspend fun getComics(): List<ComicEntity> = comicDao.getComics()
}