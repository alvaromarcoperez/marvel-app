package com.amp.marvelapp.data.features.comics.repository

import androidx.lifecycle.Transformations
import androidx.paging.Config
import androidx.paging.toLiveData
import com.amp.marvelapp.data.core.utils.Listing
import com.amp.marvelapp.data.features.comics.cache.ComicCache
import com.amp.marvelapp.data.features.comics.factory.ComicDataSourceFactory
import com.amp.marvelapp.data.features.comics.service.ComicService
import com.amp.marvelapp.data.features.user.cache.UserCache
import com.amp.marvelapp.domain.core.exception.Failure
import com.amp.marvelapp.domain.features.comics.IComicsRepository
import com.amp.marvelapp.domain.features.comics.models.ComicTopView
import com.amp.marvelapp.domain.features.comics.models.ComicView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class ComicRepository(
    private val service: ComicService,
    private val cache: ComicCache,
    private val userCache: UserCache
) : IComicsRepository {

    override fun getComics(scope: CoroutineScope, titleStartsWith: String?): Listing<ComicView> {
        val sourceFactory =
            ComicDataSourceFactory(
                coroutineScope = scope,
                titleStartsWith = titleStartsWith,
                service = service
            )
        val livePagedList = sourceFactory.toLiveData(
            config = Config(
                pageSize = 20,
                enablePlaceholders = true
            )
        )
        val refreshState =
            Transformations.switchMap(sourceFactory.sourceLiveData) { it.initialLoad }
        return Listing(
            pagedList = livePagedList,
            networkState = Transformations.switchMap(sourceFactory.sourceLiveData) { it.networkState },
            retry = { sourceFactory.sourceLiveData.value?.retryAllFailed() },
            refresh = { sourceFactory.sourceLiveData.value?.invalidate() },
            refreshState = refreshState
        )
    }

    override suspend fun doFavorite(comic: ComicView): Result<Boolean> =
        withContext(Dispatchers.IO) {
            val user = userCache.getUser()
            if (user != null) {
                // Convert to ComicEntity
                val comicEntity = comic.toEntity(user.id)

                // Save in Room
                cache.saveComic(comicEntity)

                // Add to Firestore
                service.addComic(user.email, comicEntity)

                val comicTopFirebase = comicEntity.toComicTopFirebase()

                service.updateTop(comicTopFirebase)

                service.updateShards(comicTopFirebase.id)

                Result.success(true)
            } else {
                Result.failure(Failure.NotFound)
            }
        }

    override fun getTopComics(): Flow<Result<List<ComicTopView>>> =
        service.getTopComics().map { result ->
            result.fold(
                onSuccess = { firebaseTop ->
                    // Merge with my favorites comics
                    val favorites = cache.getComics().map { it.id }

                    val comics = firebaseTop.map { topComic ->
                        val isFavorite = favorites.find { it == topComic.id } != null
                        topComic.toView(isFavorite)
                    }

                    Result.success(comics)
                },
                onFailure = { Result.failure(Failure.FirebaseError) }
            )
        }
}