package com.amp.marvelapp.data.core.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.amp.marvelapp.data.core.utils.ListConverter
import com.amp.marvelapp.data.features.characters.cache.CharacterDao
import com.amp.marvelapp.data.features.characters.models.CharacterEntity
import com.amp.marvelapp.data.features.comics.cache.ComicDao
import com.amp.marvelapp.data.features.comics.models.ComicEntity
import com.amp.marvelapp.data.features.user.cache.UserDao
import com.amp.marvelapp.data.features.user.models.UserEntity

@Database(
    entities = [UserEntity::class, CharacterEntity::class, ComicEntity::class],
    version = 1,
    exportSchema = true
)
@TypeConverters(ListConverter::class)
abstract class MyDataBase : RoomDatabase() {

    abstract fun getUserDao(): UserDao

    abstract fun getCharacterDao(): CharacterDao

    abstract fun getComicsDao(): ComicDao

}