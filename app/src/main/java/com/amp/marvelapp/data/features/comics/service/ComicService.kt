package com.amp.marvelapp.data.features.comics.service

import com.amp.marvelapp.data.core.service.BaseService
import com.amp.marvelapp.data.core.utils.Constants
import com.amp.marvelapp.data.core.utils.FirestoreCollections
import com.amp.marvelapp.data.core.utils.FirestoreCollections.COMICS_COLLECTION
import com.amp.marvelapp.data.core.utils.FirestoreCollections.TOP_COMICS_COLLECTION
import com.amp.marvelapp.data.core.utils.FirestoreCollections.USER_COLLECTION
import com.amp.marvelapp.data.features.comics.models.ComicEntity
import com.amp.marvelapp.data.features.comics.models.ComicTopFirebase
import com.amp.marvelapp.data.features.comics.models.ComicsResponse
import com.amp.marvelapp.domain.core.exception.Failure
import com.amp.marvelapp.domain.core.models.Counter
import com.amp.marvelapp.domain.core.models.Shard
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber
import kotlin.math.floor

class ComicService(
    private val api: ComicsApi,
    private val firesStore: FirebaseFirestore
) : BaseService() {

    suspend fun getComics(
        offset: Int?,
        titleStartsWith: String?
    ): Result<ComicsResponse> =
        safeCall { api.getComics(offset, titleStartsWith) }

    suspend fun addComic(userEmail: String, comicEntity: ComicEntity): Result<Boolean> =
        try {
            firesStore.collection(USER_COLLECTION)
                .document(userEmail)
                .collection(COMICS_COLLECTION)
                .document(comicEntity.id.toString())
                .set(comicEntity)
                .await()

            Result.success(true)
        } catch (exception: Exception) {
            Result.failure(Failure.FirebaseError)
        }

    suspend fun updateTop(comicTopFirebase: ComicTopFirebase): Result<Boolean> =
        try {
            val topComicsRef = firesStore.collection(TOP_COMICS_COLLECTION)
                .document(comicTopFirebase.id.toString())

            val comicDoc = topComicsRef.get().await()

            if (!comicDoc.exists()) {
                firesStore.runTransaction { transaction ->
                    // Add counter
                    transaction.set(topComicsRef, Counter(numShards = 10))

                    // Add comic
                    transaction.set(topComicsRef, comicTopFirebase)

                    // Init counter and increment random id counter
                    val randomCounter = floor(Math.random() * 10).toInt()

                    for (i in 0 until 10) {
                        val makeShard =
                            topComicsRef.collection(FirestoreCollections.SHARDS_COLLECTION)
                                .document(i.toString())

                        if (i == randomCounter) {
                            transaction.set(makeShard, Shard(1))
                        } else {
                            transaction.set(makeShard, Shard(0))
                        }
                    }

                    // Update counter in comic
                    val updateData = hashMapOf(ComicTopFirebase::totalShards.name to 1)
                    transaction.set(topComicsRef, updateData, SetOptions.merge())
                }.await()

                Result.success(true)
            } else {
                // Increment counter
                val shardId = floor(Math.random() * 10).toInt()
                val shardRef =
                    topComicsRef.collection(FirestoreCollections.SHARDS_COLLECTION)
                        .document(shardId.toString())

                shardRef.update(Shard::count.name, FieldValue.increment(1)).await()

                Result.success(true)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.FirebaseError)
        }

    suspend fun updateShards(comicId: Long): Result<Boolean> =
        try {
            val topComicsRef = firesStore.collection(TOP_COMICS_COLLECTION)
                .document(comicId.toString())

            // Get total counter
            val query =
                topComicsRef.collection(FirestoreCollections.SHARDS_COLLECTION).get().await()

            var total = 0
            query.toObjects(Shard::class.java).forEach { shard -> total += shard.count }

            // Update counter in character
            val updateData = hashMapOf(ComicTopFirebase::totalShards.name to total)

            topComicsRef.set(updateData, SetOptions.merge()).await()

            Result.success(true)
        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.FirebaseError)
        }


    // 1 .- Usamos callbackFlow como channelFlow , producimos en un buffer los elementos que
    // vamos a consumir en nuestro viewmodel
    fun getTopComics(): Flow<Result<List<ComicTopFirebase>>> = callbackFlow {

        // 2.- Creamos una referencia al documento en nuestra DB
        val subscription = firesStore
            .collection(TOP_COMICS_COLLECTION)

            // 3.- Generamos una suscripcion que va a emitir datos a nuestro consumidor, para esto
            // usamos el .addSnapshotListener para escuchar por cambios en la base de datos y
            // emitirlos con offer al consumidor en el viewmodel
            .addSnapshotListener { querySnapshot, exception ->
                Timber.d("---- ACTUALIZACION")
                if (exception == null) {
                    val comics = querySnapshot?.toObjects(ComicTopFirebase::class.java)
                        ?: emptyList<ComicTopFirebase>()

                    val top10 = comics.sortedByDescending { it.totalShards }.take(10)
                    offer(Result.success(top10))
                } else {
                    offer(Result.failure(Failure.FirebaseError))
                }
            }

        // Por último , si la UI que esta consumiendo este channel no esta mas activa (es decir,
        // el viewmodel paso por onCleared() y destruyo el collect del flow) cerramos
        // la suscripción para evitar cualquier leak
        awaitClose {
            Timber.d("---- REMOVE")
            subscription.remove()
        }
    }
}

interface ComicsApi {

    @GET("/v1/public/comics")
    suspend fun getComics(
        @Query("offset") offset: Int? = 0,
        @Query("titleStartsWith") titleStartsWith: String? = null,
        @Query("apikey") apikey: String = Constants.PUBLIC_KEY,
        @Query("hash") hash: String = Constants.HASH,
        @Query("ts") ts: Int = Constants.TS
    ): Response<ComicsResponse>

    // TODO: EXISTE AÑADIR UN PARAMETRO PARA QUE TE DEVUELVA LOS COMICS DE {ULTIMA SEMANA, ESTA SEMANA, PROXIMA SEMANA, PROXIMO MES }
}