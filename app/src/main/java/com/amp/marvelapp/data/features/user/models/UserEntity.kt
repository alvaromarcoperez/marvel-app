package com.amp.marvelapp.data.features.user.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.amp.marvelapp.domain.features.user.models.User

@Entity
data class UserEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = COLUMN_ID) var id: String = "",
    var userName: String = "",
    var email: String = "",
    var avatar: String = ""
) {

    fun toUser() = User(
        id = id,
        email = email,
        userName = userName,
        avatar = avatar
    )

    companion object {
        const val COLUMN_ID = "id"
    }
}