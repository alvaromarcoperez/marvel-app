package com.amp.marvelapp.data.core.di

import androidx.room.Room
import com.amp.marvelapp.data.core.room.MyDataBase
import com.amp.marvelapp.data.core.utils.NetworkHandler
import com.amp.marvelapp.data.features.characters.cache.CharacterCache
import com.amp.marvelapp.data.features.characters.repository.CharactersRepository
import com.amp.marvelapp.data.features.characters.service.CharacterApi
import com.amp.marvelapp.data.features.characters.service.CharacterService
import com.amp.marvelapp.data.features.comics.cache.ComicCache
import com.amp.marvelapp.data.features.comics.repository.ComicRepository
import com.amp.marvelapp.data.features.comics.service.ComicsApi
import com.amp.marvelapp.data.features.comics.service.ComicService
import com.amp.marvelapp.data.features.user.cache.UserCache
import com.amp.marvelapp.data.features.user.repository.UserRepository
import com.amp.marvelapp.data.features.user.service.UserService
import com.amp.marvelapp.domain.features.characters.ICharactersRepository
import com.amp.marvelapp.domain.features.comics.IComicsRepository
import com.amp.marvelapp.domain.features.user.IUserRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.moshi.Moshi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

const val ENDPOINT = "http://gateway.marvel.com/"

private val client = OkHttpClient.Builder()
    .connectTimeout(30, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS)
    .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
    .addInterceptor(object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            val original: Request = chain.request()

            val request: Request = original.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .method(original.method, original.body)
                .build()

            return chain.proceed(request)
        }

    })
    .build()

private val retrofit = Retrofit.Builder()
    .baseUrl(ENDPOINT)
    .client(client)
    .addConverterFactory(
        MoshiConverterFactory.create(
           // Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        )
    )
    .build()


val repositoryModule = module {
    single<ICharactersRepository> {
        CharactersRepository(
            service = get(),
            cache = get(),
            userCache = get()
        )
    }
    single<IComicsRepository> { ComicRepository(service = get(), cache = get(), userCache = get()) }
    single<IUserRepository> {
        UserRepository(cache = get(), service = get())
    }
}

val serviceModule = module {
    single { NetworkHandler(androidContext()) }
    single { FirebaseAuth.getInstance() }
    single { FirebaseFirestore.getInstance() }

    single { CharacterService(api = get(), firesStore = get()) }
    single { ComicService(api = get(), firesStore = get()) }
    single { UserService(firebaseAuth = get(), firesStore = get()) }

    single { retrofit.create(CharacterApi::class.java) }
    single { retrofit.create(ComicsApi::class.java) }
}

val cacheModule = module {
    single {
        Room.databaseBuilder(androidContext(), MyDataBase::class.java, "marvelapp")
            /*.addMigrations(Migrations.MIGRATION_1_2, Migrations.MIGRATION_2_3)*/
            .build()
    }

    single { get<MyDataBase>().getUserDao() }
    single { get<MyDataBase>().getCharacterDao() }
    single { get<MyDataBase>().getComicsDao() }


    single { UserCache(get()) }
    single { CharacterCache(get()) }
    single { ComicCache(get())}
}