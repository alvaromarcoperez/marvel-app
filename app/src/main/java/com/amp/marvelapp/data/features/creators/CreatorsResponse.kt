package com.amp.marvelapp.data.features.creators

import com.amp.marvelapp.domain.features.creators.models.CreatorContent
import com.amp.marvelapp.domain.features.creators.models.CreatorItem


data class CreatorContentResponse(
    val available: Long,
    val collectionURI: String,
    val items: List<CreatorItemResponse>,
    val returned: Long
) {
    fun toView() =
        CreatorContent(
            available = this.available,
            collectionURI = this.collectionURI,
            items = this.items.map { it.toView() },
            returned = this.returned
        )
}

data class CreatorItemResponse(
    val resourceURI: String,
    val name: String,
    val role: String
) {
    fun toView() = CreatorItem(
        resourceURI = this.resourceURI,
        name = this.name,
        role = this.role
    )
}
