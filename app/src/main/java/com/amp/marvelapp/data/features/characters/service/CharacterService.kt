package com.amp.marvelapp.data.features.characters.service

import com.amp.marvelapp.data.core.service.BaseService
import com.amp.marvelapp.data.core.utils.Constants
import com.amp.marvelapp.data.core.utils.FirestoreCollections.CHARACTER_COLLECTION
import com.amp.marvelapp.data.core.utils.FirestoreCollections.SHARDS_COLLECTION
import com.amp.marvelapp.data.core.utils.FirestoreCollections.TOP_CHARACTER_COLLECTION
import com.amp.marvelapp.data.core.utils.FirestoreCollections.USER_COLLECTION
import com.amp.marvelapp.data.features.characters.models.CharacterEntity
import com.amp.marvelapp.data.features.characters.models.CharacterResponse
import com.amp.marvelapp.data.features.characters.models.CharacterTopFirebase
import com.amp.marvelapp.domain.core.exception.Failure
import com.amp.marvelapp.domain.core.models.Counter
import com.amp.marvelapp.domain.core.models.Shard
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber
import kotlin.math.floor

class CharacterService(
    private val api: CharacterApi,
    private val firesStore: FirebaseFirestore
) : BaseService() {

    suspend fun getCharacters(
        offset: Int?,
        nameStartsWith: String?
    ): Result<CharacterResponse> =
        safeCall { api.getCharacters(offset, nameStartsWith) }

    suspend fun addCharacter(userEmail: String, characterEntity: CharacterEntity): Result<Boolean> =
        try {
            firesStore.collection(USER_COLLECTION)
                .document(userEmail)
                .collection(CHARACTER_COLLECTION)
                .document(characterEntity.id.toString())
                .set(characterEntity)
                .await()

            Result.success(true)
        } catch (exception: Exception) {
            Result.failure(Failure.FirebaseError)
        }

    suspend fun updateTop(characterTopFirebase: CharacterTopFirebase): Result<Boolean> =
        try {
            val topCharacterRef = firesStore.collection(TOP_CHARACTER_COLLECTION)
                .document(characterTopFirebase.id.toString())

            val characterDoc = topCharacterRef.get().await()

            if (!characterDoc.exists()) {
                firesStore.runTransaction { transaction ->
                    // Add counter
                    transaction.set(topCharacterRef, Counter(numShards = 10))

                    // Add character
                    transaction.set(topCharacterRef, characterTopFirebase)

                    // Init counter and increment random id counter
                    val randomCounter = floor(Math.random() * 10).toInt()

                    for (i in 0 until 10) {
                        val makeShard =
                            topCharacterRef.collection(SHARDS_COLLECTION).document(i.toString())

                        if (i == randomCounter) {
                            transaction.set(makeShard, Shard(1))
                        } else {
                            transaction.set(makeShard, Shard(0))
                        }
                    }

                    // Update counter in character
                    val updateData = hashMapOf(characterTopFirebase::totalShards.name to 1)
                    transaction.set(topCharacterRef, updateData, SetOptions.merge())
                }.await()

                Result.success(true)
            } else {
                // Increment counter
                val shardId = floor(Math.random() * 10).toInt()
                val shardRef =
                    topCharacterRef.collection(SHARDS_COLLECTION).document(shardId.toString())

                shardRef.update(Shard::count.name, FieldValue.increment(1)).await()

                Result.success(true)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.FirebaseError)
        }

    suspend fun updateShards(charaterId: Long): Result<Boolean> =
        try {
            val topCharacterRef = firesStore.collection(TOP_CHARACTER_COLLECTION)
                .document(charaterId.toString())

            // Get total counter
            val query = topCharacterRef.collection(SHARDS_COLLECTION).get().await()

            var total = 0
            query.toObjects(Shard::class.java).forEach { shard -> total += shard.count }

            // Update counter in character
            val updateData = hashMapOf(CharacterTopFirebase::totalShards.name to total)

            topCharacterRef.set(updateData, SetOptions.merge()).await()

            Result.success(true)
        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.FirebaseError)
        }

    // 1 .- Usamos callbackFlow como channelFlow , producimos en un buffer los elementos que
    // vamos a consumir en nuestro viewmodel
    fun getTopCharacters(): Flow<Result<List<CharacterTopFirebase>>> = callbackFlow {

        // 2.- Creamos una referencia al documento en nuestra DB
        val subscription = firesStore
            .collection(TOP_CHARACTER_COLLECTION)

            // 3.- Generamos una suscripcion que va a emitir datos a nuestro consumidor, para esto
            // usamos el .addSnapshotListener para escuchar por cambios en la base de datos y
            // emitirlos con offer al consumidor en el viewmodel
            .addSnapshotListener { querySnapshot, exception ->
                Timber.d("---- ACTUALIZACION")
                if (exception == null) {
                    val characters = querySnapshot?.toObjects(CharacterTopFirebase::class.java)
                        ?: emptyList<CharacterTopFirebase>()

                    val top10 = characters.sortedByDescending { it.totalShards }.take(10)
                    offer(Result.success(top10))
                } else {
                    offer(Result.failure(Failure.FirebaseError))
                }
            }

        // Por último , si la UI que esta consumiendo este channel no esta mas activa (es decir,
        // el viewmodel paso por onCleared() y destruyo el collect del flow) cerramos
        // la suscripción para evitar cualquier leak
        awaitClose {
            Timber.d("---- REMOVE")
            subscription.remove()
        }
    }

    suspend fun getExample(
        offset: Int?,
        nameStartsWith: String?
    ): Result<CharacterResponse> =
        safeCall { api.getCharacters(offset, nameStartsWith) }
}

interface CharacterApi {

    @GET("/v1/public/characters")
    suspend fun getCharacters(
        @Query("offset") offset: Int? = 0,
        @Query("nameStartsWith") nameStartsWith: String? = null,
        @Query("apikey") apikey: String = Constants.PUBLIC_KEY,
        @Query("hash") hash: String = Constants.HASH,
        @Query("ts") ts: Int = Constants.TS
    ): Response<CharacterResponse>
}