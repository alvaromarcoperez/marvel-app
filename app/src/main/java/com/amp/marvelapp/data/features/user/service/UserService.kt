package com.amp.marvelapp.data.features.user.service

import com.amp.marvelapp.data.core.utils.FirestoreCollections.USER_COLLECTION
import com.amp.marvelapp.data.features.user.models.UserEntity
import com.amp.marvelapp.domain.core.exception.Failure
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await

class UserService(
    private val firebaseAuth: FirebaseAuth,
    private val firesStore: FirebaseFirestore
) {

    suspend fun login(account: AuthCredential): Result<UserEntity> =
        try {
            // Authentication
            val data = firebaseAuth.signInWithCredential(account).await()
            val firebaseUser = data.user

            if (firebaseUser != null) {
                addUser(firebaseUser)
            } else {
                Result.failure(Failure.FirebaseError)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.FirebaseError)
        }

    suspend fun addUser(firebaseUser: FirebaseUser): Result<UserEntity> = try {
        val userEntity: UserEntity = with(firebaseUser.providerData[0]) {
            UserEntity(
                id = this.uid,
                avatar = this.photoUrl.toString(),
                email = this.email ?: "",
                userName = this.displayName ?: ""
            )
        }

        // Add firestore
        firesStore.collection(USER_COLLECTION)
            .document(userEntity.email)
            .set(userEntity)
            .await()

        Result.success(userEntity)
    } catch (exception: Exception) {
        exception.printStackTrace()
        Result.failure(Failure.FirebaseError)
    }
}