package com.amp.marvelapp.data.core.utils

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

class ListConverter {

    @TypeConverter
    fun fromString(data: String?): List<String>? {
        data?.let {
            val type = Types.newParameterizedType(List::class.java, String::class.java)
            val adapter = Moshi.Builder().build().adapter<List<String>>(type)
            return adapter.fromJson(it)
        } ?: return null
    }

    @TypeConverter
    fun fromArrayList(someObjects: List<String>?): String? {
        someObjects?.let {
            val type = Types.newParameterizedType(String::class.java, List::class.java)
            val adapter = Moshi.Builder().build().adapter<List<String>>(type)
            return adapter.toJson(it)
        } ?: return null
    }
}
