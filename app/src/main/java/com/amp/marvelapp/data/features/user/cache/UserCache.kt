package com.amp.marvelapp.data.features.user.cache

import com.amp.marvelapp.data.features.user.models.UserEntity

class UserCache(private var userDao: UserDao) {

    suspend fun getUser(): UserEntity? = userDao.getAllUsers().firstOrNull()

    suspend fun saveUser(user: UserEntity) {
        userDao.saveUser(user)
    }


}