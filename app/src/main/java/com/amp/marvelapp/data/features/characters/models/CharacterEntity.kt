package com.amp.marvelapp.data.features.characters.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.amp.marvelapp.data.features.user.models.UserEntity

@Entity(
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = arrayOf(UserEntity.COLUMN_ID),
        childColumns = arrayOf(CharacterEntity.COLUMN_USER_ID)
    )]
)
data class CharacterEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = COLUMN_ID) val id: Long = 0,
    val name: String = "",
    val description: String = "",
    val modified: String = "",
    val thumbnail: String = "",
    /*TODO: TO EXCLUDE IN FIRESTORE @get:Exclude*/
    @ColumnInfo(name = COLUMN_USER_ID, index = true) val userId: String
) {

    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_USER_ID = "userId"
    }

    fun toCharacterTopFirebase() = CharacterTopFirebase(
        id = this.id,
        name = this.name,
        description = this.description,
        modified = this.modified,
        thumbnail = this.thumbnail,
        totalShards = 0
    )
}


