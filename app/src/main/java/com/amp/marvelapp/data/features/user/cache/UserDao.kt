package com.amp.marvelapp.data.features.user.cache

import androidx.room.*
import com.amp.marvelapp.data.core.room.BaseDao
import com.amp.marvelapp.data.features.user.models.UserEntity

@Dao
interface UserDao : BaseDao<UserEntity> {

    @Query("SELECT * from UserEntity ORDER BY id ASC")
    suspend fun getAllUsers(): List<UserEntity>


    @Query("DELETE FROM UserEntity")
    suspend fun deleteAll()

    @Transaction
    suspend fun saveUser(user: UserEntity) {
        deleteAll()
        insert(user)
    }
}