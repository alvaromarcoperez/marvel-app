package com.amp.marvelapp.data.features.comics.cache

import androidx.room.Dao
import androidx.room.Query
import com.amp.marvelapp.data.core.room.BaseDao
import com.amp.marvelapp.data.features.characters.models.CharacterEntity
import com.amp.marvelapp.data.features.comics.models.ComicEntity

@Dao
interface ComicDao : BaseDao<ComicEntity> {

    @Query("SELECT * from ComicEntity")
    suspend fun getComics() : List<ComicEntity>

    @Query("SELECT * from ComicEntity WHERE id = :id")
    suspend fun getById(id: String): ComicEntity?

    @Query("DELETE FROM ComicEntity WHERE id = :id")
    suspend fun deleteById(id: String)

}