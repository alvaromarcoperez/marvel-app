package com.amp.marvelapp.data.features.characters.repository

import androidx.lifecycle.Transformations
import androidx.paging.Config
import androidx.paging.toLiveData
import com.amp.marvelapp.data.core.utils.Listing
import com.amp.marvelapp.data.features.characters.cache.CharacterCache
import com.amp.marvelapp.data.features.characters.factory.CharacterDataSourceFactory
import com.amp.marvelapp.data.features.characters.service.CharacterService
import com.amp.marvelapp.data.features.user.cache.UserCache
import com.amp.marvelapp.domain.core.exception.Failure
import com.amp.marvelapp.domain.features.characters.ICharactersRepository
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.amp.marvelapp.domain.features.characters.models.CharaterTopView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class CharactersRepository(
    private val service: CharacterService,
    private val cache: CharacterCache,
    private val userCache: UserCache
) : ICharactersRepository {

    override fun getCharacters(
        scope: CoroutineScope,
        startWithName: String?
    ): Listing<CharacterView> {
        val sourceFactory =
            CharacterDataSourceFactory(
                coroutineScope = scope,
                startWithName = startWithName,
                service = service
            )
        val livePagedList = sourceFactory.toLiveData(
            config = Config(
                pageSize = 20,
                enablePlaceholders = true
            )
        )
        val refreshState =
            Transformations.switchMap(sourceFactory.sourceLiveData) { it.initialLoad }
        return Listing(
            pagedList = livePagedList,
            networkState = Transformations.switchMap(sourceFactory.sourceLiveData) { it.networkState },
            retry = { sourceFactory.sourceLiveData.value?.retryAllFailed() },
            refresh = { sourceFactory.sourceLiveData.value?.invalidate() },
            refreshState = refreshState
        )

    }

    override suspend fun doFavorite(character: CharacterView): Result<Boolean> =
        withContext(Dispatchers.IO) {
            val user = userCache.getUser()
            if (user != null) {
                // Convert to CharacterEntity
                val characterEntity = character.toEntity(user.id)

                // Save in Room
                cache.saveCharacter(characterEntity)

                // Add to Firestore
                service.addCharacter(user.email, characterEntity)

                val characterTopFirebase = characterEntity.toCharacterTopFirebase()

                service.updateTop(characterTopFirebase)

                service.updateShards(characterTopFirebase.id)

                Result.success(true)
            } else {
                Result.failure(Failure.NotFound)
            }
        }

    override fun getTopCharacters(): Flow<Result<List<CharaterTopView>>> =
        service.getTopCharacters().map { result ->
            result.fold(
                onSuccess = { firebaseTop ->
                    // Merge with my favorites characters
                    val favorites = cache.getCharacters().map { it.id }

                    val characters = firebaseTop.map { topCharacter ->
                        val isFavorite = favorites.find { it == topCharacter.id } != null
                        topCharacter.toView(isFavorite)
                    }

                    Result.success(characters)
                },
                onFailure = { Result.failure(Failure.FirebaseError) }
            )
        }

    /* suspend fun getExample(name: String?): Result<List<ComicView>> =
         service.getExample(0, name)
             .map { data ->
                 data.characterData.results.map { it.toView() }
             }*/
}