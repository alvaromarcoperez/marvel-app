package com.amp.marvelapp.data.core.utils

object Constants {
    const val PUBLIC_KEY = "d94169b84b1a354fe89a966ae97d8cde"
    private const val PRIVATE_KEY = "6425e4249495ce3812548c183ec0e0fa9e8b0dc1"
    val HASH = "7be0d062af21becdba57899d7a2cb257"//"1$PRIVATE_KEY$PUBLIC_KEY".md5()
    const val TS = 1
}

object FirestoreCollections {
    const val USER_COLLECTION = "users"
    const val CHARACTER_COLLECTION = "characters"
    const val COMICS_COLLECTION = "comics"

    const val TOP_CHARACTER_COLLECTION="top_characters"
    const val TOP_COMICS_COLLECTION ="top_comics"

    const val SHARDS_COLLECTION ="shards"
}