package com.amp.marvelapp.data.features.comics.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.amp.marvelapp.data.core.utils.NetworkState
import com.amp.marvelapp.data.features.comics.service.ComicService
import com.amp.marvelapp.data.features.comics.models.ComicsResponse
import com.amp.marvelapp.domain.features.comics.models.ComicView
import kotlinx.coroutines.*

class ComicDataSource(
    private val coroutineScope: CoroutineScope,
    private val service: ComicService,
    private val titleStartsWith: String?
) : PageKeyedDataSource<Int, ComicView>() {

    private var supervisorJob = SupervisorJob()

    // keep a function reference for the retry event
    private var retry: (() -> Any)? = null

    /**
     * There is no sync on the state because paging will always call loadInitial first then wait
     * for it to return some success value before calling loadAfter.
     */
    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    //retry failed request
    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.invoke()
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ComicView>
    ) {
        coroutineScope.launch(Dispatchers.IO) {
            try {
                networkState.postValue(NetworkState.LOADING)
                initialLoad.postValue(NetworkState.LOADING)

                callGetComics(0)
                    .onSuccess { response ->
                        retry = null
                        networkState.postValue(NetworkState.LOADED)
                        initialLoad.postValue(NetworkState.LOADED)
                        callback.onResult(
                            response.comicsData.results.map { it.toView() },
                            null,
                            1
                        )
                    }
                    .onFailure {
                        retry = { loadInitial(params, callback) }
                        val error = NetworkState.error("unknown error")
                        networkState.postValue(error)
                        initialLoad.postValue(error)
                    }
            } catch (exception: Exception) {
                retry = { loadInitial(params, callback) }
                val error = NetworkState.error(exception.message ?: "unknown error")
                networkState.postValue(error)
                initialLoad.postValue(error)
            }
        }
    }


    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ComicView>) {
        coroutineScope.launch(Dispatchers.IO) {
            try {
                networkState.postValue(NetworkState.LOADING)

                callGetComics(params.key * params.requestedLoadSize)
                    .onSuccess { response ->
                        retry = null
                        callback.onResult(
                            response.comicsData.results.map { it.toView() },
                            params.key.inc()
                        )
                        networkState.postValue(NetworkState.LOADED)
                    }
                    .onFailure {
                        retry = { loadAfter(params, callback) }
                        val error = NetworkState.error("unknown error")
                        networkState.postValue(error)
                        initialLoad.postValue(error)
                    }
            } catch (exception: Exception) {
                retry = { loadAfter(params, callback) }
                val error = NetworkState.error(exception.message ?: "unknown error")
                networkState.postValue(error)
                initialLoad.postValue(error)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ComicView>) {
    }

    override fun invalidate() {
        super.invalidate()
        supervisorJob.cancelChildren()
    }

    private suspend fun callGetComics(offset: Int): Result<ComicsResponse> =
        service.getComics(offset, titleStartsWith)
}