package com.amp.marvelapp.data.core.utils

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import okio.Buffer
import timber.log.Timber
import java.io.IOException

class LoggingInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader("Content-Type", "application/json")
            .addHeader("Accept", "application/json")
            .build()

        val t1 = System.nanoTime()
        var requestLog =
            "Sending request ${request.url} on ${chain.connection()}%n${request.headers}"

        if (request.method.compareTo("post", ignoreCase = true) == 0) {
            requestLog = requestLog + "  " + bodyToString(request)
        }
        Timber.d("Retrofit request\n$requestLog")

        val response = chain.proceed(request)
        val t2 = System.nanoTime()

        val responseLog = String.format(
            "Received response for %s in %.1fms%n%s",
            response.request.url,
            (t2 - t1) / 1e6,
            response.headers
        )

        val bodyString = response.body?.string() ?: ""

        Timber.d("Retrofit response\n$responseLog$bodyString")

        return response.newBuilder()
            .body(ResponseBody.create(response.body?.contentType(), bodyString))
            .build()
    }

    private fun bodyToString(request: Request): String = try {
        val copy = request.newBuilder().build()
        val buffer = Buffer()
        copy.body?.writeTo(buffer)
        buffer.readUtf8()
    } catch (e: IOException) {
        "did not work"
    }
}