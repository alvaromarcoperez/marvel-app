package com.amp.marvelapp.data.features.characters.models

import com.amp.marvelapp.data.core.models.ResourceContentResponse
import com.amp.marvelapp.data.core.models.ThumbnailResponse
import com.amp.marvelapp.data.core.models.URLResponse
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CharacterResponse(
    /*@Json(name = "code") val code: Long = 0,
    @Json(name = "status") val status: String = "",
    @Json(name = "copyright") val copyright: String = "",
    @Json(name = "attributionText") val attributionText: String = "",
    @Json(name = "attributionHTML") val attributionHTML: String = "",
    @Json(name = "etag") val etag: String = "",*/
    @Json(name = "data") val characterData: CharacterDataResponse
)

@JsonClass(generateAdapter = true)
data class CharacterDataResponse(
    @Json(name = "offset") val offset: Long = 0,
    @Json(name = "limit") val limit: Long = 0,
    @Json(name = "total") val total: Long = 0,
    @Json(name = "count") val count: Long = 0,
    @Json(name = "results") val results: List<CharacterResultResponse>
)

@JsonClass(generateAdapter = true)
data class CharacterResultResponse(
    @Json(name = "id") val id: Long = 0,
    @Json(name = "name") val name: String = "",
    @Json(name = "description") val description: String = "",
    @Json(name = "modified") val modified: String = "",
    @Json(name = "thumbnail") val thumbnail: ThumbnailResponse,
    @Json(name = "resourceURI") val resourceURI: String = "",
    @Json(name = "comics") val comics: ResourceContentResponse,
    @Json(name = "series") val series: ResourceContentResponse,
    @Json(name = "stories") val stories: ResourceContentResponse,
    @Json(name = "events") val events: ResourceContentResponse,
    @Json(name = "urls") val urls: List<URLResponse> = emptyList()
) {
    fun toView(): CharacterView =
        CharacterView(
            id = id,
            name = name,
            description = description,
            modified = modified,
            thumbnail = thumbnail.getThumbnailPath(),
            isFavorite = false
        )
}



