package com.amp.marvelapp.data.features.comics.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.amp.marvelapp.data.features.user.models.UserEntity

@Entity/*(
    foreignKeys = [ForeignKey(
        entity = UserEntity::class,
        parentColumns = arrayOf(UserEntity.COLUMN_ID),
        childColumns = arrayOf(ComicEntity.COLUMN_USER_ID)
    )]
)*/
data class ComicEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = COLUMN_ID) val id: Long = 0,
    val title: String = "",
    val variantDescription: String = "",
    val description: String? = null,
    val modified: String = "",
    val pageCount: Long = 0,
    val thumbnail: String = ""
) {

    companion object {
        const val COLUMN_ID = "id"
        const val COLUMN_USER_ID = "userId"
    }

    fun toComicTopFirebase() = ComicTopFirebase(
        id = this.id,
        title = this.title,
        variantDescription = this.variantDescription,
        description = this.description,
        modified = this.modified,
        pageCount = this.pageCount,
        thumbnail = this.thumbnail,
        totalShards = 0
    )
}


