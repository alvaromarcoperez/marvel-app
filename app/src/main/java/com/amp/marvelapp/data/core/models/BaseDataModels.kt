package com.amp.marvelapp.data.core.models

import com.amp.marvelapp.domain.core.models.ResourceContent
import com.amp.marvelapp.domain.core.models.ResourceItem
import com.amp.marvelapp.domain.core.models.URLContent
import com.squareup.moshi.Json

data class ResourceContentResponse(
    @Json(name = "available") val available: Long = 0,
    @Json(name = "collectionURI") val collectionURI: String = "",
    @Json(name = "items") val items: List<ResourceItemResponse> = emptyList(),
    @Json(name = "returned") val returned: Long = 0
) {
    fun toView(): ResourceContent = ResourceContent(
        available = this.available,
        collectionURI = collectionURI,
        items = this.items.map { it.toView() },
        returned = this.returned
    )
}

data class ResourceItemResponse(
    @Json(name = "resourceURI") val resourceURI: String = "",
    @Json(name = "name") val name: String = "",
    @Json(name = "type") val type: String? = null
) {
    fun toView() = ResourceItem(
        resourceURI = this.resourceURI,
        name = this.name,
        type = this.type
    )
}

data class ThumbnailResponse(
    @Json(name = "path") val path: String = "",
    @Json(name = "extension") val extension: String = ""
){
    fun getThumbnailPath(): String = if (path.contains("image_not_available")) {
        ""
    } else {
        "${path}.${extension}"
    }
}

data class URLResponse(
    @Json(name = "type") val type: String = "",
    @Json(name = "url") val url: String = ""
) {
    fun toView() = URLContent(type = this.type, url = this.url)
}
