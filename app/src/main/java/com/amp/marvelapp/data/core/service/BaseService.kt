package com.amp.marvelapp.data.core.service

import com.amp.marvelapp.data.core.utils.NetworkHandler
import com.amp.marvelapp.domain.core.exception.Failure
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.Response
import timber.log.Timber

abstract class BaseService : KoinComponent {

    private val networkHandler by inject<NetworkHandler>()

    suspend fun <T : Any> safeCall(
        call: suspend () -> Response<T>
    ): Result<T> {
        return when (networkHandler.isConnected) {
            true -> safeApiResult(call)
            false -> Result.failure(Failure.NetworkConnection)
        }
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>
    ): Result<T> {
        return try {
            val response = call.invoke()

            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    Result.success(body)
                } else {
                    Result.failure(Failure.ServerError)
                }
            } else {
                //val converterError = ErrorUtils.parseError(response)
                //converterError.code = response.code()
                Timber.e("Error ${response.code()} - Response not successful")
                Result.failure(Failure.ServerError)
            }

        } catch (exception: Exception) {
            exception.printStackTrace()
            Result.failure(Failure.ServerError)
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            Result.failure(Failure.ServerError)
        }
    }
}