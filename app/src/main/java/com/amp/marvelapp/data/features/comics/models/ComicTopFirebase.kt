package com.amp.marvelapp.data.features.comics.models

import com.amp.marvelapp.domain.features.comics.models.ComicTopView
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ComicTopFirebase(
    val id: Long = 0,
    val title: String = "",
    val description: String? = null,
    val variantDescription: String = "",
    val modified: String = "",
    val pageCount: Long = 0,
    val thumbnail: String = "",
    val totalShards: Int = 0
) {

    fun toView(isFavorite: Boolean) = ComicTopView(
        id = this.id,
        description = this.description?: "",
        name = this.title,
        totalShards = this.totalShards,
        thumbnail = this.thumbnail,
        isFavorite = isFavorite
    )
}