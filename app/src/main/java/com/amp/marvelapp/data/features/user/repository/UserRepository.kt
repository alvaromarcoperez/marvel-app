package com.amp.marvelapp.data.features.user.repository

import com.amp.marvelapp.data.features.user.cache.UserCache
import com.amp.marvelapp.data.features.user.service.UserService
import com.amp.marvelapp.domain.core.exception.Failure
import com.amp.marvelapp.domain.features.user.IUserRepository
import com.amp.marvelapp.domain.features.user.models.User
import com.google.firebase.auth.AuthCredential
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(
    private val cache: UserCache,
    private val service: UserService

) : IUserRepository {

    override suspend fun getUser(): Result<User> = withContext(Dispatchers.IO) {
        val user = cache.getUser()
        if (user != null) {
            Result.success(user.toUser())
        } else {
            Result.failure<User>(Failure.NotFound)
        }
    }

    override suspend fun login(account: AuthCredential): Result<User> =
        withContext(Dispatchers.IO) {
            service.login(account)
                .onSuccess { cache.saveUser(it) }
                .map { it.toUser() }
        }


}