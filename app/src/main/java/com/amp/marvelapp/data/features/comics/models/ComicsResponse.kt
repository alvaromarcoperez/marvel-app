package com.amp.marvelapp.data.features.comics.models

import com.amp.marvelapp.data.core.models.ResourceContentResponse
import com.amp.marvelapp.data.core.models.ResourceItemResponse
import com.amp.marvelapp.data.core.models.ThumbnailResponse
import com.amp.marvelapp.data.core.models.URLResponse
import com.amp.marvelapp.data.features.creators.CreatorContentResponse
import com.amp.marvelapp.domain.features.comics.models.ComicView
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ComicsResponse(
    /*@Json(name = "code") val code: Long = 0,
    @Json(name = "status") val status: String = "",
    @Json(name = "copyright") val copyright: String = "",
    @Json(name = "attributionText") val attributionText: String = "",
    @Json(name = "attributionHTML") val attributionHTML: String = "",
    @Json(name = "etag") val etag: String = "",*/
    @Json(name = "data") val comicsData: ComicDataResponse
)

@JsonClass(generateAdapter = true)
data class ComicDataResponse(
    val offset: Long = 0,
    val limit: Long = 0,
    val total: Long = 0,
    val count: Long = 0,
    val results: List<ComicResultResponse>
)

@JsonClass(generateAdapter = true)
data class ComicResultResponse(
    val id: Long,
    val digitalId: Long,
    val title: String,
    val issueNumber: Long,
    val variantDescription: String,
    val description: String? = null,
    val modified: String,
    val isbn: String,
    val upc: String,
    val diamondCode: String,
    val ean: String,
    val issn: String,
    val format: String,
    val pageCount: Long,
    val textObjects: List<TextObjectResponse>,
    val resourceURI: String,
    val urls: List<URLResponse>,
    val series: ResourceItemResponse,
    val variants: List<ResourceItemResponse>,
    val collections: List<Any?>,
    val collectedIssues: List<ResourceItemResponse>,
    val dates: List<DateResponse>,
    val prices: List<PriceResponse>,
    val thumbnail: ThumbnailResponse,
    val images: List<ThumbnailResponse>,
    val creators: CreatorContentResponse,
    val characters: ResourceContentResponse,
    val stories: ResourceContentResponse,
    val events: ResourceContentResponse
) {
    fun toView() = ComicView(
        id = this.id,
        description = this.description ?: "",
        thumbnail = this.thumbnail.getThumbnailPath(),
        title = this.title,
        pageCount = this.pageCount,
        modified = this.modified,
        variantDescription = this.variantDescription,
        isFavorite = false
    )
}

@JsonClass(generateAdapter = true)
data class DateResponse(
    val type: String,
    val date: String
)

@JsonClass(generateAdapter = true)
data class PriceResponse(
    val type: String,
    val price: Double
)

@JsonClass(generateAdapter = true)
data class TextObjectResponse(
    val type: String,
    val language: String,
    val text: String
)
