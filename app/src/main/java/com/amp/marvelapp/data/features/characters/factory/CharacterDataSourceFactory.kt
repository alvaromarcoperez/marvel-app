package com.amp.marvelapp.data.features.characters.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.amp.marvelapp.data.features.characters.service.CharacterService
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import kotlinx.coroutines.CoroutineScope

class CharacterDataSourceFactory(
    private val coroutineScope: CoroutineScope,
    private val service: CharacterService,
    private val startWithName: String?
) :
    DataSource.Factory<Int, CharacterView>() {

    val sourceLiveData = MutableLiveData<CharacterDataSource>()

    override fun create(): DataSource<Int, CharacterView> {
        val source =
            CharacterDataSource(
                coroutineScope,
                service,
                startWithName
            )
        sourceLiveData.postValue(source)
        return source
    }
}