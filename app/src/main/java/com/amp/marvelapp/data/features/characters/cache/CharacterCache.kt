package com.amp.marvelapp.data.features.characters.cache

import com.amp.marvelapp.data.features.characters.models.CharacterEntity

class CharacterCache(private var characterDao: CharacterDao) {

    suspend fun saveCharacter(characterEntity: CharacterEntity) {
        characterDao.insert(characterEntity)
    }

    suspend fun deleteCharacter(characterEntity: CharacterEntity) {
        characterDao.delete(characterEntity)
    }

    suspend fun deleteCharacterById(characterId: String) {
        characterDao.deleteById(characterId)
    }

    suspend fun getCharacters(): List<CharacterEntity> = characterDao.getCharacters()

}