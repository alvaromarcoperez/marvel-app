package com.amp.marvelapp.data.features.characters.models

import com.amp.marvelapp.domain.features.characters.models.CharaterTopView
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CharacterTopFirebase(
    val id: Long = 0,
    val name: String = "",
    val description: String = "",
    val modified: String = "",
    val thumbnail: String = "",
    val totalShards: Int = 0
) {

    fun toView(isFavorite: Boolean = false) =
        CharaterTopView(
            id = id,
            name = name,
            description = description,
            thumbnail = thumbnail,
            isFavorite = isFavorite
        )
}