package com.amp.marvelapp.domain.core.models

data class ResourceContent(
    val available: Long = 0,
    val collectionURI: String = "",
    val items: List<ResourceItem> = emptyList(),
    val returned: Long = 0
)

data class ResourceItem(
    val resourceURI: String = "",
    val name: String = "",
    val type: String? = null
)

data class URLContent(
    val type: String = "",
    val url: String = ""
)

// counters/${ID}
data class Counter(var numShards: Int, var total: Int = 0)

// counters/${ID}/shards/${NUM}
class Shard() {
    var count: Int = 0

    constructor(count: Int) : this() {
        this.count = count
    }
}