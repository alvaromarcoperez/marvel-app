package com.amp.marvelapp.domain.features.user

import com.amp.marvelapp.domain.features.user.models.User
import com.google.firebase.auth.AuthCredential


interface IUserRepository {

    suspend fun getUser(): Result<User>

    suspend fun login(account: AuthCredential): Result<User>


}