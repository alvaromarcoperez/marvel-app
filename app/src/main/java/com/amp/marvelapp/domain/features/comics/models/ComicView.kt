package com.amp.marvelapp.domain.features.comics.models

import com.amp.marvelapp.data.features.comics.models.ComicEntity

data class ComicView(
    val id: Long = 0,
    val title: String = "",
    val description: String = "",
    val variantDescription: String = "",
    val modified: String = "",
    val pageCount: Long = 0,
    val thumbnail: String = "",
    var isFavorite: Boolean = false
) {
    fun toEntity(userId: String) = ComicEntity(
        id = id,
        title = title,
        description = description,
        variantDescription = variantDescription,
        modified = modified,
        pageCount = pageCount,
        thumbnail = thumbnail
    )
}

data class ComicTopView(
    val id: Long = 0,
    val name: String = "",
    val description: String = "",
    val thumbnail: String = "",
    val totalShards: Int = 0,
    val isFavorite: Boolean = false
)