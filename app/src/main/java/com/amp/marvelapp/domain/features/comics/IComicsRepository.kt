package com.amp.marvelapp.domain.features.comics

import com.amp.marvelapp.data.core.utils.Listing
import com.amp.marvelapp.domain.features.comics.models.ComicTopView
import com.amp.marvelapp.domain.features.comics.models.ComicView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface IComicsRepository {

    fun getComics(scope: CoroutineScope, titleStartsWith : String?): Listing<ComicView>

    suspend fun doFavorite(comic: ComicView) : Result<Boolean>

    fun getTopComics() : Flow<Result<List<ComicTopView>>>

}