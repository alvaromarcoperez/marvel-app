package com.amp.marvelapp.domain.features.characters

import com.amp.marvelapp.data.core.utils.Listing
import com.amp.marvelapp.domain.features.characters.models.CharacterView
import com.amp.marvelapp.domain.features.characters.models.CharaterTopView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface ICharactersRepository {

    fun getCharacters(scope: CoroutineScope, startWithName : String?): Listing<CharacterView>

    suspend fun doFavorite(comic: CharacterView) : Result<Boolean>

    fun getTopCharacters() : Flow<Result<List<CharaterTopView>>>

    //suspend fun getExample(name : String?) : Result<List<Character>>
}