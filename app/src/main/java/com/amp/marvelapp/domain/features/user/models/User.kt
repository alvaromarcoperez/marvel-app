package com.amp.marvelapp.domain.features.user.models

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
open class User(
    var id: String = "",
    var email: String = "",
    var userName: String = "",
    var avatar: String = ""
) : Parcelable