package com.amp.marvelapp.domain.core.exception

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific failure should extend [FeatureFailure] class.
 */
sealed class Failure(val exception: Exception? = null) : Exception(){
    object NetworkConnection : Failure()
    object ServerError : Failure()
    object NotFound : Failure()

    /** Extend this class for feature specific failures.*/
    class FeatureFailure(featureException: Exception = Exception("Feature error")) : Failure(featureException)

    object FirebaseError : Failure()


}