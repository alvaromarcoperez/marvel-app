package com.amp.marvelapp.domain.features.characters.models

import com.amp.marvelapp.data.features.characters.models.CharacterEntity
import com.amp.marvelapp.domain.core.models.ResourceContent
import com.amp.marvelapp.domain.core.models.URLContent

data class CharacterView(
    val id: Long = 0,
    val name: String = "",
    val description: String = "",
    val modified: String = "",
    val thumbnail: String = "",
    var isFavorite: Boolean = false
) {
    fun toEntity(userId: String) = CharacterEntity(
        id = id,
        name = name,
        description = description,
        thumbnail = thumbnail,
        modified = modified,
        userId = userId
    )
}

data class CharaterTopView(
    val id: Long = 0,
    val name: String = "",
    val description: String = "",
    val thumbnail: String = "",
    val totalShards: Int = 0,
    val isFavorite: Boolean = false
)