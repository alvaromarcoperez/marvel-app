package com.amp.marvelapp.domain.features.creators.models

data class CreatorContent(
    val available: Long,
    val collectionURI: String,
    val items: List<CreatorItem>,
    val returned: Long
)

data class CreatorItem(
    val resourceURI: String,
    val name: String,
    val role: String
)
