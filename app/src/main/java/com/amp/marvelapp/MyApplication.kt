package com.amp.marvelapp

import android.app.Application
import com.amp.marvelapp.data.core.di.cacheModule
import com.amp.marvelapp.data.core.di.repositoryModule
import com.amp.marvelapp.data.core.di.serviceModule
import com.amp.marvelapp.ui.core.di.fragmentModule
import com.amp.marvelapp.ui.core.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.fragment.koin.fragmentFactory
import org.koin.core.context.startKoin
import timber.log.Timber
import timber.log.Timber.DebugTree


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initKoin()

        initTimber()
    }

    private fun initKoin() {

        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            fragmentFactory()

            modules(
                listOf(
                    fragmentModule,
                    viewModelModule,
                    repositoryModule,
                    serviceModule,
                    cacheModule
                )
            )
        }

    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
    }
}