# Marvel App - Example Clean Architecture with differents libraries

## Kotlin 100%

## Architecture
The architecture of the project follows the principles of Clean Architecture

We have 3 main packages and a extension package:
### ui
Contains activities, fragments and adapters

### domain
Contains connection between ui and data packages and the models that the ui need.

### data
Contains a data information. This module follows the principles of Repository Pattern

### extensions
Is a extra module with extensions class

## Libraries
- MVVM
- Coroutines
- Koin
- Retrofit
- Okio
- Moshi
- Room
- Glide
- Firebase
- Timber
- LeakCanary 
- Paging

